# README #
 splicing\_map\_plants
===
####Description

It's a compilation of programs and scripts for identify and analyses splicing motifs on plants.

####Citing

**Jose A Corona-Gomez, Irving J Garcia-Lopez, Peter F Stadler, Selene L Fernandez-Valverde**

*"Splicing conservation signals in plant long non-coding RNAs"*

bioRxiv 588954; doi: https://doi.org/10.1101/588954



#### SOFTWARE REQUIREMENTS ###

Summary of set up:

The program are powered by python and need next libraries: 
 
>(os, re, sys, numpy, pandas, csv, scandir, Biopython)

External programs:
> [STAR](https://github.com/alexdobin/STAR)


> [MaxEntScan](http://genes.mit.edu/burgelab/maxent/)


> [HalTools](https://github.com/ComparativeGenomicsToolkit/hal/blob/master/README.md)

---
###Methodology for extract splicing motives and generating splicing map 
---

####Transcriptomes

It is necessary to have transcripts of good quality and depth if you want to look for lncRNAs.
And have reads of transcriptome in fastq format example: *Arabidopsis thaliana* 
transcriptomes from [GEA](https://www.ebi.ac.uk/gxa/home):


```
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR505/SRR505743/SRR505743.fastq.gz

wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR505/SRR505744/SRR505744.fastq.gz

wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR505/SRR505745/SRR505745.fastq.gz

wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR505/SRR505746/SRR505746.fastq.gz

```
*note: This transcriptomes correspond to reference (Liu et al., 2012).*


---
####Align reads to reference genome using  `STAR`(Dobin et al., 2013)

Requirements for mapping transcriptomes:


1.	Reference genome in fasta format

2.	Transcriptomes

3.	Install `STAR`

4.	Optional you can use genomic annotation for reference genome in gff format



Generate index:


`STAR --runThreadN 4 --runMode alignReads --genomeDir GenomearaTha-index –readFilesIn  araTha/SRR505743.fastq.gz --readFilesCommand gunzip -c`


Generate alignment:


`STAR --runThreadN 4 --runMode alignReads --genomeDir GenomearaTha-index --readFilesIn araTha/SRR505743.fastq.gz --readFilesCommand gunzip -c --outFileNamePrefix STAR-mapping/Align-out/SRR505743.Aligned.out.sam`

*Repeat these two steps for each transcriptome.*

---
####Extract predicted introns

You need file table produced for the transcriptome alignment STAR *.out.tab* and reference genome in fasta format example:

>	Put all results *.out.tab* in new directory example `Dir-out-tab\`


>	Transform this results in table:


`python Extractor-splice-motives.py Arabidopsis_thaliana-TAIR10-genome.fa SRR505744.Aligned.out.samLog.out`

`Extractor-splice-motives.py` take predictions of each intron and filter this for: size (Long intron \> 59 and Long intron \< 1000), unique mapped reads per 
intron (\>9) and minimum of 100 reads per intron. The size of intron was established for *Arabidopsis thaliana* for avoid false positives, 
that have a minimum size of intron between 60 pb and 1000pb based on work of (Marquez et al., 2012). This filters could be modified in code.


Concatenate all results of `Extractor-splice-motives.py` if you have more than one transcriptome and generate unique BED6 file:


`python ConcatSplicingMotives.py Dir-out-tab/`



`ConcatSplicingMotives.py` generate a new table with introns and splicing motives and erase duplications, between transcriptomes. This mean only one splice
site per genomic position. The results are a BED6 files that contain each splicing site annotate for transcriptomes separate in two classes: 
one for donor and other for acceptor. This program only take splicing motives that correspond to canonical splicing junctions (*'GT-AG','CT-AC'*) excluding
the rest.

---

####Cut whole genomic alignment (WGA) using `Hal Tools` (Hickey et al., 2013)


Requirements for cut WGA:

>Install `HAL` toolkit

>Have WGA in `HAL` format

>Bed6 file for splicing sites



Example in WGA of 16 Brassica species:


```
r=("Brassica_napus" "Brassica_oleracea" "Brassica_rapa" "Raphanus_sativus" "Eutrema_parvulum" "Eutrema_salsugineum" "Sisymbrium_irio" "Arabis_alpina" "Leavenworthia_alabamica" "Boechera_stricta" "Camelina_sativa" "Capsella_rubella" "Arabidopsis_lyrata" "Arabidopsis_halleri" "Aethionema_arabicum")

change BED6D5ss for output of splicing sites o for file to do liftover
for i in "${r[@]}"; do
        echo $i
        halLiftover 16WGA-brasicaceas.hal Arabidopsis_thaliana BED6D5ss.bed $i $i-Lift-BED6D5ss.bed
done

for i in "${r[@]}"; do
        echo $i
        halLiftover 16WGA-brasicaceas.hal Arabidopsis_thaliana BED6A3ss.bed $i $i-Lift-BED6A3ss.bed
done

```

*The result is a bed file of ortholog positions in each specie in WGA, if this position exist in WGA.*

---

####Extract nucleotide sequences of splicing sites

Script `MultiSplicngMapFilterCut.py` filter for size BED files and do cutting of splicing sites in genomes the input information for this program is the output of `Liftover.sh`.
In addition this program can process multi BED Files and create a big data base.
After use script is necessary create a table in format csv, that have in the first column: direction of Hal liftover result in BED6, second column direction of genome and 
third column type of splicing site 3ss or 5ss.


Example table content:


```
Liftover/Aethionema_arabicum-Lift-BED6A3ss.bed,Genomes/Aethionema_arabicum,3ss

Liftover/Aethionema_arabicum-Lift-BED6D5ss.bed,Genomes/Aethionema_arabicum,5ss

Liftover/Arabidopsis_halleri-Lift-BED6A3ss.bed,Genomes/Arabidopsis_halleri,3ss

Liftover/Arabidopsis_halleri-Lift-BED6D5ss.bed,Genomes/Arabidopsis_halleri,5ss
```

Execute script:

`python MultiSplicngMapFilterCut.py ListaLiftoversGenomes.csv`

*Result of `MultiSplicngMapFilterCut.py` is a Table in format `pikle`  that will be used for the next programs and copy in tsv format for visualization.
Script exclude: partial sequences, sequences with gaps or ambiguous nucleotides.


---

####Generate values of max entropy escore (MES) using of `MaxEntScan` (Yeo and Burge, 2004)

`MaxEntScan` program needs a splicing sites in fasta format, this could be generate with script SplicingMapPkl.py, this separates the 3ss and 5ss motifs 
into two tables, and create two fasta files with the motives.

Execute script:

`python SplicingMapPkl.py SplicingMap.pkl`

Next step is run scripts of `MaxEnt`:

```

perl score3.pl 3ss-Splicingmotives.fa > maxent-3ss-Splicingmotives.txt 

perl score5.pl 5ss-Splicingmotives.fa > maxent-5ss-Splicingmotives.txt

```

*We used `MaxEntScan` default train because we use canonical motives that are the same in humans.*

---

####Generate table that include splicing sites information and MES

Script `ConcatSplicingMdfMaxEntR.py` integrate MES with tables of splicing sites positions and species.


Execute script:


```

python ConcatSplicingMdfMaxEntR.py maxent-5ss-Splicingmotives.txt MDF5.pkl

python ConcatSplicingMdfMaxEntR.py maxent-3ss-Splicingmotives.txt MDF3.pkl

```

*The final table is information of ortholog splicing sites in each species in WGA and his MES, the basis of splicing map*

---

####**Bibliography**

Dobin, A. et al. (2013) ‘STAR: ultrafast universal RNA-seq aligner.’, Bioinformatics (Oxford, England). Oxford University Press, 29(1), pp. 15–21. doi: 10.1093/bioinformatics/bts635.

Hickey, G. et al. (2013) ‘HAL: A hierarchical format for storing and analyzing multiple genome alignments’, Bioinformatics, 29(10), pp. 1341–1342. doi: 10.1093/bioinformatics/btt128.

Liu, J. et al. (2012) ‘Genome-wide analysis uncovers regulation of long intergenic noncoding RNAs in Arabidopsis’, Plant Cell, 24(11), pp. 4333–4345. doi: 10.1105/tpc.112.102855.

Marquez, Y. et al. (2012) ‘Transcriptome survey reveals increased complexity of the alternative splicing landscape in Arabidopsis’, Genome Research, 22(6), pp. 1184–1195. doi: 10.1101/gr.134106.111.

Yeo, G. and Burge, C. B. (2004) ‘Maximum Entropy Modeling of Short Sequence Motifs with Applications to RNA Splicing Signals’, Journal of Computational Biology, 11(2–3), pp. 377–394. doi: 10.1089/1066527041410418.

---


### Contact ###

Jose Antonio Corona-Gomez
jose.corona@cinvestav.mx

