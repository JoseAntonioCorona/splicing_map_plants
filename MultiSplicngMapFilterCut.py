#!/bin/python

''' 
Program MultiSplicngMapFilterCut.py
This program filter for size BED files and do cutting of splicing sites in genomes
the input information for this program is the output of Liftover.sh 
in adition this program can procese multi BEDFiles and create a big data base
Powered by Python

$ python  MultiSplicngMapFilterCut.py ListaLiftoversGenomes.csv
J. Antonio Corona
09/08/2018
'''

import os
import re
import sys
import numpy as np
import pandas as pd
import csv
import Bio
from Bio import SeqIO

#Read BED files and list of species
try:
    TABLE = sys.argv[1]
    
except:
    sys.exit("ERROR. Can't read supplied CSV Table with columns: \"BED liftover file\",\"Genome\",\"Splicing motive\"")

#TABLE='arastrand.csv'
'''
#exmple of input table
Liftover/Aethionema_arabicum-Lift-BED6A3ss.bed,Genomes/Aethionema_arabicum,3ss
Liftover/Aethionema_arabicum-Lift-BED6D5ss.bed,Genomes/Aethionema_arabicum,5ss
Liftover/Arabidopsis_halleri-Lift-BED6A3ss.bed,Genomes/Arabidopsis_halleri,3ss
Liftover/Arabidopsis_halleri-Lift-BED6D5ss.bed,Genomes/Arabidopsis_halleri,5ss
Liftover/Arabidopsis_lyrata-Lift-BED6A3ss.bed,Genomes/Arabidopsis_lyrata,3ss
Liftover/Arabidopsis_lyrata-Lift-BED6D5ss.bed,Genomes/Arabidopsis_lyrata,5ss
'''


#funcion para convertir una funcion que tome el BED y el genoma y regrese la tabla de resultados

def SizeFilter(bed,f):
	BED=bed
	DF= pd.DataFrame.from_csv(BED, sep='\t',index_col=None,header=None)
	#print (DF)
	#names = ['ID','Sp','chrom','start','end','name','score','stran']
	DF.columns = ['chrom','start','end','name','score','stran']
	DF=DF.dropna()     #drop all rows that have any NaN values
	#print (DF.dtypes)
	DF['start'] =  DF['start'].astype('int32')
	DF['end'] =  DF['end'].astype('int32')
	DF['score'] =  DF['score'].astype('int32')
	DF['chrom'] =  DF['chrom'].astype('object')
	#print (DF.describe())
	Long = DF['end']-DF['start']
	#print (DF[['start','end']], Long)
	#print (Long.describe())
	DF['Long'] = Long	#se agrega la columna del lago de la secuencia
	print(len(DF), ' Number of splcing site in BED')
	#---------------filtar por largo------------------
	#preguntar si los motivos son s3 o s5 o
	try:
	    mot = f
	except:
	    sys.exit("ERROR. Can't read Splicing motive only permited \'3ss\'or \'5ss\' in the third column")
	if mot == '5ss':
		df=DF[DF['Long']==9]
		print(len(df), ' Number of splcing site in BED pased fiter sice (9)')
	if mot == '3ss':
		df=DF[DF['Long']==23]
		print(len(df), ' Number of splcing site in BED pased filter sice (23)')
	if mot != '5ss' and mot != '3ss':
		sys.exit("ERROR. Can't read Splicing motive only permited 3ss or 5ss ")
	return(df)


def GenomeCut(Genome,bed):
	df=bed
	#Read genomme
	try:
	    Gonome = Genome
	    
	except:
	    sys.exit("ERROR. Can't read supplied Genome ")
	
	#Crear funcion de corte en genoma nombre de especie 
	def CutSeq(row):
		x=row['start']
		y=row['end']
		z=row['chrom']
		a=y-x
		s=row['stran']
		#print(x,y,z)
		if s == '+':
			n=seq_record[x:y]
		if s == '-':
			n=seq_record[x:y]
			n = n.reverse_complement()
		if s == '.':
			n=seq_record[x:y]
		n=str(n)
		#print(a)
		return(n)
	
	genomedb = {}
	for seq_record in SeqIO.parse(Gonome, "fasta"):
	    #print(seq_record.id)
	    CHR=seq_record.id
	    CHR=str(CHR)
	    #print(repr(seq_record.seq))
	    genomedb[CHR] = seq_record.seq
	    #print(len(seq_record)
	#Craar Dataframe basio
	Head = ('chrom','start','end','name','score','stran','Long','sseq')
	MDF = pd.DataFrame(columns =  Head)
	
	df=df.set_index('chrom')
	#print(df)
	UIndex = df.index.tolist()
	#print (set(UIndex))
	UIndex = set(UIndex)
	#UIndex = str(UIndex)
	print('Cunting genome in fragment:')
	for l1 in UIndex:
		#l1 = str(l1)
		print (l1)
		dfchr = df.xs(l1)
		#print (dfchr)
		#print (dfchr.count)
		l1 = str(l1)
		if l1 in genomedb:
			seq_record = genomedb[l1]
			#print (seq_record)
			dfchr=dfchr.reset_index()
			#print (l1)
			#print (len(dfchr.index))
			marc= (len(dfchr.columns))
			if marc == 7:
				#print (dfchr)
				dfchr['sseq']=dfchr.apply(CutSeq, axis=1) # recorriendo columnas
				#print (dfchr[['name','sseq']])
				#print (dfchr)
				MDF = pd.concat([MDF,dfchr])
			else:
				print(l1+' eliminated by data inconsistency ',Genome)
	
		else:
			print(l1, 'Warning: chr not found in the genome ',Genome)
	
	#print (MDF)
	return(MDF)

Head = ('chrom','start','end','name','score','stran','Long','sseq','sp','ss')
FDF=pd.DataFrame(columns =  Head)
TAB=open(TABLE,'r')
#print (TAB)
for i in TAB:
	i=i.strip()
	i=i.split(',')
	print(i)
	DF=SizeFilter(i[0],i[2])
	DF=GenomeCut(i[1],DF)
	#agregar columna sp y motivo
	sp=i[1].split('/')
	DF.loc[:,'sp']=sp[-1]
	DF.loc[:,'ss']=i[2]
	print (DF)
	FDF = pd.concat([FDF,DF])

FDF.to_pickle('SplicingMap.pkl')
FDF.to_csv('SplicingMap.tsv',sep='\t',index=False,header=True)