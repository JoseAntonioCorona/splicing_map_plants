#!/bin/python
''' 
Program SplicingMapPkl.py
This program takes the splice table,
filters the ambiguous sequences of the motifs,
separates the 3ss and 5ss motifs into two tables,
and create two fasta files with the motives

Powered by Python
python SplicingMapPkl.py SplicingMap.pkl
J. Antonio Corona
09/08/2018
'''

import os
import re
import sys
import numpy as np
import pandas as pd
import csv

try:
    pkld = sys.argv[1]
    
except:
    sys.exit("ERROR. Can't read directory of pkl files ")

MDF = pd.read_pickle(pkld)
#print(MDF)

MDF['ambiguos']=MDF.sseq.str.contains(r"[^ATGC]")
MDF=MDF[MDF['ambiguos']==False]
del MDF['ambiguos']
print (MDF)

out ='3ss-Splicingmotives.fa'
Splseq = open(out,mode = 'w')

MDF3 = MDF[MDF['ss']=='3ss']
MDF3 = MDF3.set_index('ss')
MDF3 = MDF3.reset_index()
for l2 in MDF3.index:
	name =MDF3['name'][l2]
	name = '>'+name+'\n'
	#print (name)
	Splseq.write(name)
	sseq =MDF3['sseq'][l2]
	sseq = sseq+'\n'
	Splseq.write(sseq)				
	#print (sseq) 
Splseq.close()

out ='5ss-Splicingmotives.fa'
Splseq = open(out,mode = 'w')

MDF5 = MDF[MDF['ss']=='5ss']
MDF5 = MDF5.set_index('ss')
MDF5 = MDF5.reset_index()
for l2 in MDF5.index:
	name =MDF5['name'][l2]
	name = '>'+name+'\n'
	#print (name)
	Splseq.write(name)
	sseq =MDF5['sseq'][l2]
	sseq = sseq+'\n'
	Splseq.write(sseq)				
	#print (sseq) 
Splseq.close()

print (MDF3)
MDF3.to_pickle('MDF3.pkl')
print (MDF5)
MDF5.to_pickle('MDF5.pkl')
