''' 
Program ConcatSplicingMdfMaxEntR.py
This program integrate maxent results with tables of spplicing sites positions and sp
Powered by Python 2.7
$ python ConcatSplicingMdfMaxEntR.py maxent-5ss-Splicingmotives.txt MDF5.pkl
$ python ConcatSplicingMdfMaxEntR.py maxent-3ss-Splicingmotives.txt MDF3.pkl
J. Antonio Corona
12/07/2018
'''

import os
import re
import sys
import numpy as np
import pandas as pd
import csv

#Leer archivo de Resultado de Maxima Entropia  
try:
    MER = sys.argv[1]
    
except:
    sys.exit("ERROR. Can't read supplied MaxEnt result .txt ")

#print MER

MDF= pd.DataFrame.from_csv(MER, sep='\t',index_col=None,header=None)
MDF.columns = ['mseq','maxent']
print (MDF)

#Leer archivo Pickle

try:
    pkld = sys.argv[2]
    
except:
    sys.exit("ERROR. Can't read  pkl files ")

print ('Open----> '+pkld)
PKLl=pkld

SMDF = pd.read_pickle(PKLl) #to load .pkl back to the dataframe df
print (SMDF)

#Concatenar dos tablas 

DF = pd.concat([SMDF, MDF], axis=1, join='inner')
print(DF)


#Guardar archivo resultante
PKL='maxent-'+pkld
DF.to_pickle(PKL) 