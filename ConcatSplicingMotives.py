'''
ConcatSplicingMotives.py
This program concat results of STAR motives do it with program Extractor-splice-motives.py if you have more than
1 transcriptome

Powered by Python 2.7
$ python ConcatSplicingMotives.py
J. Antonio Corona
01/08/2018
'''

import os
import re
import sys
import numpy as np
import pandas as pd
import csv
import scandir

from os import getcwd
from scandir import scandir

def ls(ruta = getcwd()):
    return [arch.name for arch in scandir(ruta) if arch.is_file()]

#Leer lista de documentos en directorio


try:
    pkld = sys.argv[1]
    
except:
    sys.exit("ERROR. Can't read directory of splicing tables ")


lista_arq = ls(pkld) 
print (lista_arq)

#Craar Dataframe basio

#"Chr","First-base","Last-base","Strand","Intron-motif","Anoted","Unique-mapreads","Multi-mapreads","Max-spliced-alignament-oeverhang",
#"Intron-motif-traduction","Leng-intron","Intron-sequence","5ss {-3,+6}","3ss {-20,+3}"

#'''
MDF = pd.DataFrame()

for l1 in lista_arq:
	#print (l1)
	Text = l1.split(".")
	T= Text[0]
	#print (SPext[1])
	if Text[-1]=="tsv":
		
		T=T.replace('table-','')
		print (T)
		tsv=pkld+l1
		DF=pd.DataFrame.from_csv(tsv, sep='\t',index_col=None)
		DF.loc[:, 'Transcriptome'] = T
		MDF=pd.concat([MDF, DF])
		#print (DF)

	else:
		print (l1+" This file is not a .tsv file ") 

#MDF=MDF.reset_index()
#MDF.to_pickle('MDF-temporal.pkl')
#MDF=pd.read_pickle('MDF-temporal.pkl')


#Eliminar intrones redundates
MDF=MDF.sort_values(by=["Unique-mapreads","Max-spliced-alignament-oeverhang"], ascending=False)

print (MDF)
MDF["unique-p"]=MDF[["Chr","First-base","Last-base","Strand"]].apply(lambda x: '-'.join(x.astype(str)), axis=1)

MDFf=MDF.drop_duplicates(["unique-p"])
#MDFf.to_pickle('MDFf-temporal.pkl')
#''' #Brakets temporales para no procesar por segunda vez los archivos
#MDFf=pd.read_pickle('MDFf-temporal.pkl')
#print (MDFf)
#print (MDFf["Unique-mapreads"])


#next step is created dataframe than conected with the Cuf maf or hal file NINJA or another options
# parts of bed6 file 'chrom','start','end','name','score','stran'

#print (MDFf[["Chr","First-base","Last-base","Strand","Intron-motif"]])
#Define name of introns


BED6int=MDFf[["Chr","First-base","Last-base","Unique-mapreads","Strand","Intron-motif"]]
BED6int=BED6int[BED6int["Unique-mapreads"]>9]	#filtro de minimo un read 9
print("Filter unique meped reads >9")
# Restar index and Sort
#BED6int=BED6int.set_index("Chr")
BED6int=BED6int.sort_values(by=['Chr',"First-base","Last-base"], ascending=True)
BED6int=BED6int.reset_index()
del(BED6int['index'])
#put BED in base 0
BED6int['start']=BED6int["First-base"]-1
BED6int['end']=BED6int["Last-base"]-1
del(BED6int["First-base"]) 
del(BED6int["Last-base"])
#change strand anotation
BED6int.loc[BED6int["Strand"]==1, 'Strand']='+'
BED6int.loc[BED6int["Strand"]==2, 'Strand']='-'
BED6int.loc[BED6int["Strand"]==0, 'Strand']='.'
#change score base in unque maped reads 
Ascor =int(BED6int["Unique-mapreads"].max())
#BED6int['score']=(((BED6int["Unique-mapreads"].astype('int32'))/Ascor)*1000)
#BED6int['score']=BED6int['score'].astype('int32')
BED6int['score']=BED6int["Unique-mapreads"]
del(BED6int["Unique-mapreads"])
#Name of intros is fromo serial number in index + name of motive 
#Only motives 1 and 2 
print ('Only motives 1 and 2 ')

a=BED6int[BED6int['Intron-motif']==1]
b=BED6int[BED6int['Intron-motif']==2]
BED6int=pd.concat([a,b])
del a
del b
inttronmot={1:'GTAG',2:'CTAC',3:'GCAG',4:'CTGC',5:'ATAC',6:'GTAT',0:'noncanonical'}
for k in inttronmot:
	BED6int.loc[BED6int['Intron-motif']==k, 'Intron-motif']=inttronmot[k]
BED6int['name']=BED6int.index.astype(str)+'i'+BED6int['Intron-motif']

#create bed of introns
#sort columns
BED6int=BED6int[['Chr',"start","end","name","score","Strand"]]
print (BED6int)
BED6int.to_csv('BED6int.bed',sep='\t',index=False,header=False)

# Nota: plotear los resultados de uniquemapreads para tener una idea del comportamiento de los datos y el tamaño de intron
#Generate the beds of the motives of the donor and the acceptor
BED6D5ss=BED6int[['Chr',"name","score","Strand"]]
BED6A3ss=BED6int[['Chr',"name","score","Strand"]]
#Donor 
print ('Donor ----------------------> BED6D5ss')
BED6D5ss.loc[BED6D5ss["Strand"]=='+', 'start5']=BED6int['start']-3
BED6D5ss.loc[BED6D5ss["Strand"]=='+', 'end5']=BED6int['start']+6
BED6D5ss.loc[BED6D5ss["Strand"]=='-', 'start5']=BED6int['end']-5
BED6D5ss.loc[BED6D5ss["Strand"]=='-', 'end5']=BED6int['end']+4
BED6D5ss.loc[BED6D5ss["Strand"]=='.', 'start5']=BED6int['start']-3
BED6D5ss.loc[BED6D5ss["Strand"]=='.', 'end5']=BED6int['start']+6

BED6D5ss['start5']=BED6D5ss['start5'].astype(int)
BED6D5ss['end5']=BED6D5ss['end5'].astype(int)

BED6D5ss=BED6D5ss[['Chr','start5','end5',"name","score","Strand"]]
#Delete duplicates splicing sites 
#Sort 
BED6D5ss=BED6D5ss.sort_values(by=["score"], ascending=False)
#print(BED6D5ss)
#Drop
BED6D5ss["unique-p"]=BED6D5ss[['Chr','start5','end5',"Strand"]].apply(lambda x: '-'.join(x.astype(str)), axis=1)
BED6D5ss=BED6D5ss.drop_duplicates(["unique-p"])
del(BED6D5ss["unique-p"])
#re-sort
BED6D5ss=BED6D5ss.sort_values(by=['Chr','start5','end5'], ascending=True)

#save
BED6D5ss.to_csv('BED6D5ss.bed',sep='\t',index=False,header=False)
print(BED6D5ss)
#print(BED6int)

#Aceptor
print ('Aceptor  ----------------------> BED6A3ss')
BED6A3ss.loc[BED6A3ss['Strand']=='+','start3']=BED6int['end']-19
BED6A3ss.loc[BED6A3ss['Strand']=='+','end3']=BED6int['end']+4
BED6A3ss.loc[BED6A3ss['Strand']=='-','start3']=BED6int['start']-3
BED6A3ss.loc[BED6A3ss['Strand']=='-','end3']=BED6int['start']+20
BED6A3ss.loc[BED6A3ss['Strand']=='.','start3']=BED6int['end']-19
BED6A3ss.loc[BED6A3ss['Strand']=='.','end3']=BED6int['end']+4

BED6A3ss['start3']=BED6A3ss['start3'].astype(int)
BED6A3ss['end3']=BED6A3ss['end3'].astype(int)


BED6A3ss=BED6A3ss[['Chr','start3','end3',"name","score","Strand"]]
#Delete duplicates splicing sites 
#Sort 
BED6A3ss=BED6A3ss.sort_values(by=["score"], ascending=False)
#print(BED6D5ss)
#Drop
BED6A3ss["unique-p"]=BED6A3ss[['Chr','start3','end3',"Strand"]].apply(lambda x: '-'.join(x.astype(str)), axis=1)
BED6A3ss=BED6A3ss.drop_duplicates(["unique-p"])
del(BED6A3ss["unique-p"])
#re-sort
BED6A3ss=BED6A3ss.sort_values(by=['Chr','start3','end3'], ascending=True)

BED6A3ss.to_csv('BED6A3ss.bed',sep='\t',index=False,header=False)
print(BED6A3ss)