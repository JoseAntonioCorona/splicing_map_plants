#!/bin/python

''' 
Program SplicngMapFilterCut.py
This program filter for size BED files and do cutting of splicing sites in genomes
the input information for this program is the output of Liftover.sh 
Powered by Python

$ python SplicngMapFilterCut.py BEDs-Splicing/Arabidopsis_thaliana-SplicingMap-ml-5ss.txt.bed GENOMS/Arabidopsis_thaliana 5ss
J. Antonio Corona
08/08/2018
'''

import os
import re
import sys
import numpy as np
import pandas as pd
import csv
import Bio
from Bio import SeqIO

#Leer archivo Bed de sitios de splicing  
try:
    BED = sys.argv[1]
    
except:
    sys.exit("ERROR. Can't read supplied BED splicing file ")

#proces DataFrame

DF= pd.DataFrame.from_csv(BED, sep='\t',index_col=None,header=None)

#print (DF)
#names = ['ID','Sp','chrom','start','end','name','score','stran']
DF.columns = ['chrom','start','end','name','score','stran']
#DF=DF.set_index('name') #change index for name
DF=DF.dropna()     #drop all rows that have any NaN values
#print (DF.dtypes)

DF['start'] =  DF['start'].astype('int32')
DF['end'] =  DF['end'].astype('int32')
DF['score'] =  DF['score'].astype('int32')
DF['chrom'] =  DF['chrom'].astype('object')
#print (DF.describe())
Long = DF['end']-DF['start']
#print (DF[['start','end']], Long)
#print (Long.describe())
DF['Long'] = Long	#se agrega la columna del lago de la secuencia
print(len(DF), ' Number of splcing site in BED')
#---------------filtar por largo------------------
#preguntar si los motivos son s3 o s5 o
try:
 
    mot = sys.argv[3]
    
except:
    sys.exit("ERROR. Can't read Splicing motive only permited 3ss or 5ss ")

if sys.argv[3] == '5ss':
	df=DF[DF['Long']==9]
	print(len(df), ' Number of splcing site in BED pased fiter sice (9)')
if sys.argv[3] == '3ss':
	df=DF[DF['Long']==23]
	print(len(df), ' Number of splcing site in BED pased filter sice (23)')
if sys.argv[3] != '5ss' and sys.argv[3] != '3ss':
	sys.exit("ERROR. Can't read Splicing motive only permited 3ss or 5ss ")


#Read genome
try:
    Gonome = sys.argv[2]
    
except:
    sys.exit("ERROR. Can't read supplied Genome ")


genomedb = {}
for seq_record in SeqIO.parse(Gonome, "fasta"):
    #print(seq_record.id)
    CHR=seq_record.id
    CHR=str(CHR)
    #print(repr(seq_record.seq))
    genomedb[CHR] = seq_record.seq
    #print(len(seq_record))
#Crear funcion de corte en genoma nombre de especie 
def CutSeq(row):
	x=row['start']
	y=row['end']
	z=row['chrom']
	a=y-x
	#print(x,y,z)
	n=seq_record[x:y]
	n=str(n)
	#print(a)
	return(n)

#documento de salida
genome=Gonome.split("/")
out =mot+'-'+genome[-1]+'-Splicingmotives'+'.fa'
Splseq = open(out,mode = 'w')		#Archivo de salida

#Craar Dataframe basio
Head = ('chrom','start','end','name','score','stran','Long','sseq')
MDF = pd.DataFrame(columns =  Head)

df=df.set_index('chrom')
#print(df)
UIndex = df.index.tolist()
#print (set(UIndex))
UIndex = set(UIndex)
#UIndex = str(UIndex)
for l1 in UIndex:
	#l1 = str(l1)
	print (l1)
	dfchr = df.xs(l1)
	#print (dfchr)
	#print (dfchr.count)
	l1 = str(l1)
	if l1 in genomedb:
		seq_record = genomedb[l1]
		#print (seq_record)
		dfchr=dfchr.reset_index()
		#print (l1)
		#print (len(dfchr.index))
		marc= (len(dfchr.columns))
		if marc == 7:
			#print (dfchr)
			dfchr['sseq']=dfchr.apply(CutSeq, axis=1) # recorriendo columnas
			#print (dfchr[['name','sseq']])
			#print (dfchr)
			MDF = pd.concat([MDF,dfchr])
			for l2 in dfchr.index:
				#print (l2)
				
				name =dfchr['name'][l2]
				name = '>'+name+'\n'
				#print (name)
				Splseq.write(name)
				sseq =dfchr['sseq'][l2]
				sseq = sseq+'\n'
				Splseq.write(sseq)
				#print (sseq) 
		else:
			print(l1+' eliminated by data inconsistency')

	else:
		print(l1, 'Warning: chr not found in the genome')

print (MDF)
#name of outdataframe 
dfout = mot+'-'+genome[-1]+'.pkl'
MDF.to_pickle(dfout) #Data frame guardado para proximo analisis en formato de lectura rapida .pkl

Splseq.close()