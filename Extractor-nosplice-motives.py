
#$ \python
#Extraer Motivos negativos de empalme usando resultados de STAR y genoma de Referencia
#uso $ python Extractor-xsplice-motives.py genome.fa splice-juctions.tsv
#J.Antonio Corona gocanjo@hotmail.com 
# python Extractor-nosplice-motives.py Arabidopsis_thaliana-TAIR10-genome-edit-chr-name.fa General-Aligned.out.samSJ.out.tab



#importar librerias

import re
import sys
import Bio
from Bio import SeqIO
from random import randint

#leer archivo de entrada
try:
    Gonome = sys.argv[1]
    
except:
    sys.exit("ERROR. Can't read supplied Genome ")
try:
	SpliceJuctions = sys.argv[2]
except:
	sys.exit("ERROR. Can't read supplied splice-juctions tab format")

# Input Files

print (Gonome)
genome = open(Gonome, mode='r')
print (genome)

print (SpliceJuctions)
splicetsv = open(SpliceJuctions, mode='r')
print (splicetsv)


#Output files
archivo0= 'table-' + SpliceJuctions + ".tsv"
result0 = open(archivo0, mode='w+')

archivo1= 'motive5-' + SpliceJuctions + ".fasta"
result5 = open(archivo1, mode='w')

archivo2= 'motive3-' + SpliceJuctions + ".fasta"
result3 = open(archivo2, mode='w')

archivo3= 'negative-motive5-' + SpliceJuctions + ".fasta"
result5n = open(archivo3, mode='w')

archivo4= 'negative-motive3-' + SpliceJuctions + ".fasta"
result3n = open(archivo4, mode='w')

#Made header for results 0

encabezado ="Chr"+'\t'+"First-base"+'\t'+"Last-base"+'\t'+"Strand"+'\t'+"Intron-motif"+'\t'+"Anoted"+'\t'+"Unique-mapreads"+ '\t'+"Multi-mapreads"
encabezado = encabezado +'\t'+"Max-spliced-alignament-oeverhang"+'\t'+"Intron-motif-traduction"+'\t'+"Leng-intron"+'\t'+"Intron-sequence"+'\t'
encabezado = encabezado +"5ss {-3,+6}"+'\t'+"3ss {-20,+3}"+'\n'
result0.write(encabezado)


#Read genome

genomedb = {}
for seq_record in SeqIO.parse(Gonome, "fasta"):
    print(seq_record.id)
    print(repr(seq_record.seq))
    genomedb[seq_record.id] = seq_record.seq
    print(len(seq_record))



#Read SpliceJuctions Tsv
intronmotifdb = {0: "Nonnon-canonical", 1: "GT/AG", 2: "CT/AC", 3: "GC/AG", 4: "CT/GC", 5: "AT/AC", 6: "GT/AT"}

dicdb = {}
for l1 in splicetsv:
	l1s = l1.rstrip('\n')
	result0.write(l1s)
	l1s = l1s.split('\t')
	#print (l1s[0])
	intronmoif = intronmotifdb[int(l1s[4])]
	#print (intronmoif)
	result0.write('\t' + intronmoif)
	if l1s[0] in genomedb:
		#print (seq_record.id + " = " + l1s[0])
		#print (l1s[1]+ ","+ l1s[2])
		seq_record = genomedb[l1s[0]]
		firsbase = int(l1s[1])
		lastbase = int(l1s[2])
		intron = seq_record[(firsbase - 1):lastbase]
		#print (intron)
		#print (len(intron))
		Lengintron = str((len(intron)))
		result0.write('\t' + Lengintron)
		intron_str = str(repr(intron))
		result0.write('\t' + intron_str)
		#primer motivo
		ss5 = seq_record[(firsbase -  4):(firsbase + 6)]
		ss5_str = str(ss5)
		result0.write('\t' + ss5_str.lower())
		#segundo motivo
		ss3 = seq_record[(lastbase - 20):(lastbase + 3)]
		ss3_str = str(ss3)
		result0.write('\t' + ss3_str.lower())
	else:
		print(l1s[0] + 'Warning: chr not found in the genome')

	result0.write('\n')

print ('Positive motives --------------------------------------> ')
countmotives = 0
countmotivesf = 0
#Read table result 0

result0.seek(0)
for l2 in result0:
 	#print (l2)
 	l2s = l2.rstrip()
 	l2s = l2s.split('\t')
 	#print (l2s)
 	#---------Filtros de  seleccion de motivos----------#
 	#>Tamaño de intron; (plants min 60 nt medium size 160nt) 
 	#>Reads unicos mapeados en el sistema 
 	if l2s[10] != "Leng-intron": 
 		#print (l2s[10])
 		countmotives = countmotives + 1
 		Lengintron = int(l2s[10])
 		uniquereads = int(l2s[6])
 		intronmtf = int(l2s[4])
 		strand = int(l2s[3])
 		staticsintron = int(l2s[8])
 		anotatedread = int(l2s[5])
 		if Lengintron > 80 and Lengintron < 250 and strand == 1 and staticsintron > 49: 				#Intron size and direction
 			#print ('intron size f  =  ' + l2s[10])
 			if uniquereads > 10 and anotatedread == 1 and intronmtf == 1:		#unique reads anotated read and mitve
 				#print ('intron uniquereads f  =  ' + l2s[6])
 				#fastaname5ss = '> 5ssC' + l2s[0] + 'F' + l2s[1] + 's' + l2s[3] + '\n'
 				fastaname5ss = '> 5' + '\n'
 				result5.write(fastaname5ss.lower())
 				result5.write(l2s[12])
 				result5.write('\n')
 				#fastaname3ss = '> 3ssC' + l2s[0] + 'L' + l2s[2] + 's' + l2s[3] + '\n'
 				fastaname3ss = '> 3' + '\n'
 				result3.write(fastaname3ss.lower())
 				result3.write(l2s[13])
 				result3.write('\n')
 				countmotivesf = countmotivesf + 1
 	#print (fastaname5ss)
 	#print (fastaname3ss)

result5.close()
result3.close()
print ('Negative motives --------------------------------------> ')
result0.seek(0)
dic_aceptor={} #Diccionarios para motivos negativod
dic_recepor={}
#keys = genomedb.keys(0)
#print (keys) 
chromonumber = 'C1'  #<--------Se necesita el nombre del primero cromosoma o la primera secuencia seimpre sera omitida 
intronstarL = []
intronendL = []
c=0
for l3 in result0:
 	#print (l3)
 	l3s = l3.rstrip()
 	l3s = l3s.split('\t')
 	c=c+1
 	#print (l3s)
 	#---------Filtros de  seleccion de motivos negativos---------#
 	#>No deben coincidir con ninguna posición de splicing identificada por START
 	#>Deben de ser de una posición aleatorio del genoma
 	if l3s[10] != "Leng-intron": 
 	    intronstar = int(l3s[1]) #cordenadas para corte de motivos negativos
 	    intronend = int(l3s[2])
 	    if chromonumber == l3s[0]:  		#llebnar diccionarios con listas de motivos
 	        #print(intronstar)
 	        if not intronstar in intronstarL:
 	        	a=1
 	        	intronstarL+=[intronstar]
 	        if not intronend in intronendL:
 	        	a+=2
 	        	intronendL+=[intronend]
 	    else:
 		    #cambio de cromosoma
 		    #print(intronstarL)
 		    dic_aceptor[chromonumber]= intronstarL
 		    dic_recepor[chromonumber]= intronendL
 		    chromonumber = l3s[0]
 		    intronstarL = []
 		    intronendL = []

dic_aceptor[chromonumber]= intronstarL
dic_recepor[chromonumber]= intronendL

print('numero de datos en tabla result0 = ',c)

print(len(dic_aceptor))
print(len(dic_recepor))

x=0
print('dic_aceptor --')
for k1 in dic_aceptor:
	y = len(dic_aceptor[k1])
	print (y)
	x=x+y
	#print(k1)
	seq_record = genomedb[k1]
	chrl=len(seq_record)
	i=1
	while i <= 200:
		ranp=randint(5,chrl)
		if not ranp in dic_aceptor[k1]:
			#print ('random numero = ',ranp)
			ss5 = seq_record[(ranp -  4):(ranp + 6)]
			ss5_str = str(ss5)
			#print (ss5_str)
			#fastah = '> 5ssC' + str(k1) + 'P' + str(ranp)  + '\n'
			fastah = '> 0' + '\n'
			result5n.write(fastah)
			result5n.write(ss5_str.lower())
			result5n.write('\n')
			i+=1

print('sumatoria dic_aceptor',x)

x=0
print('dic_recepor --')
for k1 in dic_recepor:
	y = len(dic_aceptor[k1])
	print (y)
	x=x+y
	#print(k1)
	seq_record = genomedb[k1]
	chrl=len(seq_record)
	i=1
	while i <= 100:
		ranp=randint(21,chrl)
		if not ranp in dic_aceptor[k1]:
			#print ('random numero = ',ranp)
			ss3 = seq_record[(ranp -  20):(ranp + 3)]
			ss3_str = str(ss3)
			#print (ss5_str)
			#fastah = '> 3ssC' + str(k1) + 'P' + str(ranp)  + '\n'
			fastah = '> 0' + '\n'
			result3n.write(fastah)
			result3n.write(ss3_str.lower())
			result3n.write('\n')
			i+=1
print('sumatoria dic_recepor',x)



print ('Filters > fasta Output')
print ('Lengintron > 80 and Lengintron < 250 and strand == 1 and staticsintron > 49')
print ('uniquereads > 10 and anotatedread == 1 and intronmtf == 1')
print ('Total motives = '+ str(countmotives))
print ('Total motives Filter= '+ str(countmotivesf))

result5n.close()
result3n.close()
result0.close()
