
#Extraer datos de Motivos de empalme usando resultados de STAR y genoma de Referencia
#uso $ python Extractor-xsplice-motives.py genome.fa splice-juctions.tsv
#J.Antonio Corona gocanjo@hotmail.com 
# python Extractor-splice-motives.py Arabidopsis_thaliana-TAIR10-genome-edit-chr-name.fa SRR505744.Aligned.out.samLog.out



#importar librerias

import re
import sys
import Bio
from Bio import SeqIO

#leer archivo de entrada
try:
    Gonome = sys.argv[1]
    
except:
    sys.exit("ERROR. Can't read supplied Genome ")
try:
	SpliceJuctions = sys.argv[2]
except:
	sys.exit("ERROR. Can't read supplied splice-juctions tab format")

# Input Files

print (Gonome)
genome = open(Gonome, mode='r')
print (genome)

print (SpliceJuctions)
splicetsv = open(SpliceJuctions, mode='r')
print (splicetsv)


#Output files
archivo0= 'table-' + SpliceJuctions + ".tsv"
result0 = open(archivo0, mode='w+')

archivo1= 'motive5-' + SpliceJuctions + ".fasta"
result5 = open(archivo1, mode='w')

archivo2= 'motive3-' + SpliceJuctions + ".fasta"
result3 = open(archivo2, mode='w')

#Made header for results 0

encabezado ="Chr"+'\t'+"First-base"+'\t'+"Last-base"+'\t'+"Strand"+'\t'+"Intron-motif"+'\t'+"Anoted"+'\t'+"Unique-mapreads"+ '\t'+"Multi-mapreads"
encabezado = encabezado +'\t'+"Max-spliced-alignament-oeverhang"+'\t'+"Intron-motif-traduction"+'\t'+"Leng-intron"+'\t'+"Intron-sequence"+'\t'
encabezado = encabezado +"5ss {-3,+6}"+'\t'+"3ss {-20,+3}"+'\n'
result0.write(encabezado)


#Read genome

genomedb = {}
for seq_record in SeqIO.parse(Gonome, "fasta"):
    print(seq_record.id)
    print(repr(seq_record.seq))
    genomedb[seq_record.id] = seq_record.seq
    print(len(seq_record))



#Read SpliceJuctions Tsv
intronmotifdb = {0: "Nonnon-canonical", 1: "GT/AG", 2: "CT/AC", 3: "GC/AG", 4: "CT/GC", 5: "AT/AC", 6: "GT/AT"}

dicdb = {}
for l1 in splicetsv:
	l1s = l1.rstrip('\n')
	result0.write(l1s)
	l1s = l1s.split('\t')
	#print (l1s[0])
	intronmoif = intronmotifdb[int(l1s[4])]
	#print (intronmoif)
	result0.write('\t' + intronmoif)
	if l1s[0] in genomedb:
		#print (seq_record.id + " = " + l1s[0])
		#print (l1s[1]+ ","+ l1s[2])
		seq_record = genomedb[l1s[0]]
		firsbase = int(l1s[1])
		lastbase = int(l1s[2])
		intron = seq_record[(firsbase - 1):lastbase]
		#print (intron)
		#print (len(intron))
		Lengintron = str((len(intron)))
		result0.write('\t' + Lengintron)
		intron_str = str(repr(intron))
		result0.write('\t' + intron_str)
		#primer motivo
		ss5 = seq_record[(firsbase -  4):(firsbase + 6)]
		ss5_str = str(ss5)
		result0.write('\t' + ss5_str)
		#segundo motivo
		ss3 = seq_record[(lastbase - 20):(lastbase + 3)]
		ss3_str = str(ss3)
		result0.write('\t' + ss3_str)
	else:
		print(l1s[0] + 'Warning: chr not found in the genome')

	result0.write('\n')


countmotives = 0
countmotivesf = 0
#Read table result 0
result0.seek(0)
for l2 in result0:
 	#print (l2)
 	l2s = l2.rstrip()
 	l2s = l2s.split('\t')
 	#print (l2s)
 	#---------Filtros de  seleccion de motivos----------#
 	#>Tamaño de intron; (plants min 60 nt medium size 160nt) 
 	#>Reads unicos mapeados en el sistema 
 	if l2s[10] != "Leng-intron": 
 		#print (l2s[10])
 		countmotives = countmotives + 1
 		Lengintron = int(l2s[10])
 		uniquereads = int(l2s[6])
 		anotatedread = int(l2s[5])
 		if Lengintron > 59 and Lengintron < 1000: 									#Intron size
 			#print ('intron size f  =  ' + l2s[10])
 			if uniquereads > 99:					#unique reads anotated read
 				#print ('intron uniquereads f  =  ' + l2s[6])
 				fastaname5ss = '> 5ssC' + l2s[0] + 'F' + l2s[1] + 's' + l2s[3] + '\n'
 				result5.write(fastaname5ss)
 				result5.write(l2s[12])
 				result5.write('\n')
 				fastaname3ss = '> 3ssC' + l2s[0] + 'L' + l2s[2] + 's' + l2s[3] + '\n'
 				result3.write(fastaname3ss)
 				result3.write(l2s[13])
 				result3.write('\n')
 				countmotivesf = countmotivesf + 1
 	#print (fastaname5ss)
 	#print (fastaname3ss)
print ('Filters > fasta Output')
print ('Lengintron > 59 and Lengintron < 1000: #Intron size')
print ('uniquereads > 99:')
print ('Total motives = '+ str(countmotives))
print ('Total motives Filter= '+ str(countmotivesf))
result5.close()
result3.close()
result0.close()
