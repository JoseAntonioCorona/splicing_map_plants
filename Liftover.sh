#!/bin/bash

#module load hal/2.1
#module load kenUtils/v302

#prove hal alignament file
#halStats 16WGA-brasicaceas.hal
#halValidate 16WGA-brasicaceas.ha

#species list 

r=("Brassica_napus" "Brassica_oleracea" "Brassica_rapa" "Raphanus_sativus" "Eutrema_parvulum" "Eutrema_salsugineum" "Sisymbrium_irio" "Arabis_alpina" "Leavenworthia_alabamica" "Boechera_stricta" "Camelina_sativa" "Capsella_rubella" "Arabidopsis_lyrata" "Arabidopsis_halleri" "Aethionema_arabicum")

#change BED6D5ss for output of splicing sites o for file to do liftover
for i in "${r[@]}"; do
        echo $i
        halLiftover 16WGA-brasicaceas.hal Arabidopsis_thaliana BED6D5ss.bed $i $i-Lift-BED6D5ss.bed
done

for i in "${r[@]}"; do
        echo $i
        halLiftover 16WGA-brasicaceas.hal Arabidopsis_thaliana BED6A3ss.bed $i $i-Lift-BED6A3ss.bed
done